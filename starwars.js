"use strict"




/*
jQuery Ajax Star Wars
For this project, go to GitLab and create a new project name 'ajax-starWars'.(make sure you make this project public) Once created, clone the repo and Get from Version Control on IntelliJ. Run your git commands and connect IntelliJ to GitLab.
    Create a new html named 'starWars.html'. Copy the code from starWars.html (on slack)
Using the Star Wars api
https://swapi.dev/
    Render the requirement data onto the html file
*Retrieve at least the first 10 characters
**You will either use $.ajax() or $.get(), and a .done() callback **
Add Bootstrap to your webpage with some custom CSS to make the style your own
** Be Creative!**
*/


$.ajax("https://swapi.dev/api/people/",    {
    type: "get",

}).done(function (data) {
   /* console.log(data)
    console.log(data.results[0].name)*/

     var dataHTML = displayusers(data.results);

     $("#insertCharacters").html(dataHTML);
});


function displayusers(insertCharacters) {

    var usersOnHTML = '';

    insertCharacters.forEach(function (user) {
        console.log(user)


        usersOnHTML += `
        <div class="user">
            <h3>Movie Character: ${user.name}</h3>
            <br>
            <ul>
                 <li>
                      <P>Height: ${user.height}</P>
                </li>
                <li>
                      <p>Mass: ${user.mass}</p>
                </li>
                <li>
                       <p>Birth Year ${user.birth_year}</p>
                </li>
                <li>
                       <p>Gender ${user.gender}</p>
                </li>
                <li>
                       <p>eye_color ${user.eye_color}</p>
                </li>
                <li>
                       <p>Skin Color ${user.skin_color}</p>
                </li>
                <li>
                       <p>Created ${user.created}</p>
                </li>
                <li>
                       <p>Edited ${user.edited}</p>
                </li>
                <li>
                       <p>Starships ${user.starships}</p>
                </li>
                <li>
                       <p>url ${user.url}</p>
                </li>
            </ul>
        </div>
        `      })

    return usersOnHTML;
    }







// $.ajax("My-star-wars.json")

